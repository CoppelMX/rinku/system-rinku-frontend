import api from './api'

const service = {}
const URL = '/movements'

service.getMovements = async (month, year) => {
  return api
    .get(`${URL}`, { params: { month, year}}).then((res) => res.data)
}

service.registerMovement = async (month, year, employeeId, deliveries) => {
  return api.post(`${URL}`, { month, year, employeeId, deliveries }).then((res) => res.data)
}

export default service