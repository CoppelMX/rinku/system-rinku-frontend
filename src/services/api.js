const BASE_URL = import.meta.env.VITE_APP_ENV_BASE_URL
import axios from 'axios'
import { useSesionUsuario } from '../stores/user'
const sesionUsuario = useSesionUsuario()

const api = axios.create({
  baseURL: BASE_URL
})

api.interceptors.request.use(function (config) {
  const token = sesionUsuario.getToken
  if (token) {
    const authString = `Bearer ${token}`
    config.headers.Authorization = authString
  }

  return config
})

export default api
