import api from './api'
import { useSesionUsuario } from '../stores/user'
const service = {}
const URL = '/auth'

service.login = async function (user, password) {
  const session = await api.post(`${URL}/login`, { user, password }).then((res) => res.data)
  if (session.error == false) {
    const sesionUsuario = useSesionUsuario()
    sesionUsuario.setSession(session.data)
    return { error: false, session: session.data }
  } else return session
}

export default service
