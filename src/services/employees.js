import api from './api'

const service = {}
const URL = '/employee'

service.getByNoEmployee = async (noEmployee) => {
  return api.get(`${URL}`, { params: { noEmployee }}).then((res) => res.data)
}

service.getEmployees = async (search, id) => {
  return api.get(`${URL}/${id}`, { params: { search }}).then((res) => res.data)
}

service.registerEmployee = async (noEmployee, name, rolId) => {
  return api.post(`${URL}/`, { noEmployee, name, rolId }).then((res) => res.data)
}

service.updateEmployee = async (id, datos) => {
  return api.put(`${URL}/${id}`, { datos }).then((res) => res.data)
}

export default service
