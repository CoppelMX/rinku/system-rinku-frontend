import api from './api'

const service = {}
const URL = '/roles'

service.getRoles = async () => {
  return api.get(`${URL}`).then((res) => res.data)
}

export default service
