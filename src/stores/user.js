import { defineStore } from 'pinia'

export const useSesionUsuario = defineStore({
  id: 'sesionUsuario',
  state: () => ({
    session: null
  }),
  getters: {
    getSession: (state) => state.session,
    getNombreUsuario(state) {
      if (state.session) {
        return `${state.session.nombre ? state.session.nombre : ''}`
      }
      return ''
    },
    getDescripcionRol(state) {
      if (state.session) {
        return state.session.rol
      }
      return ''
    },
    getToken(state) {
      if (state.session) {
        return state.session.token
      }
      return null
    }
  },
  actions: {
    setSession(sesion) {
      sessionStorage.setItem('session', JSON.stringify(sesion))
      this.$patch((state) => {
        state.session = sesion
      })
    },
    cerrarSesion() {
      this.$reset()
      sessionStorage.removeItem('session')
    }
  }
})
