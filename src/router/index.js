import { createRouter, createWebHistory } from 'vue-router'
import { useSesionUsuario } from '../stores/user'

const routes = [
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Login.vue'),
    meta: {
      title: 'Login',
      isPublic: true
    }
  },
  {
    path: '/employees',
    name: 'employees',
    component: () => import('../views/Employees.vue'),
    meta: {
      title: 'Employees',
      isPublic: false
    }
  },
  {
    path: '/movements',
    name: 'movements',
    component: () => import('../views/Movements.vue'),
    meta: {
      title: 'Movements',
      isPublic: false
    }
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  document.title = `Rinku ${to.meta.title ? '| ' + to.meta.title : ''}`

  const isPublic = to.meta.isPublic || false
  const sesionUsuario = useSesionUsuario()
  let isAuthenticated = sesionUsuario.session || false

  if (!isAuthenticated) {
    const datosSession = sessionStorage.getItem('session')
    if (datosSession) {
      sesionUsuario.setSession(JSON.parse(datosSession))
      isAuthenticated = true
    }
  }

  if (!isPublic && !isAuthenticated) {
    return next({ name: 'login' })
  }

  if (!isPublic && isAuthenticated) {
    return next()
  }

  if (to.name === 'login' && isAuthenticated) {
    return next({ name: 'employees' })
  }

  return next()
})

export default router
